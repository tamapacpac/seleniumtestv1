package test;

import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pageObjects.SearchPage;

public class SearchPageTest {
	
	static WebDriver driver;
	
	public static void main(String[] args) throws InterruptedException {
		Test1();
	}
	
	@Test
	public static void Test1() throws InterruptedException {
		
		// path for the chromedriver
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\trish\\OneDrive\\Desktop\\automation\\chromedriver_win32\\chromedriver.exe");
						
		driver = new ChromeDriver();
						
		// opening the browser
		driver.get("http://www.just-eat.co.uk/");
		driver.manage().window().maximize();
		
		SearchPage searchPage =  new SearchPage(driver);
		searchPage.acceptCookies();
		searchPage.searchBox("AR51 1AA");
		searchPage.clickSearch();
		searchPage.checkArea51();
		
		// closing the browser
		driver.quit();
	}

}
