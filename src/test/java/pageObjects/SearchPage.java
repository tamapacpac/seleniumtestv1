package pageObjects;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class SearchPage {
	
	WebDriver driver;
		
	public SearchPage(WebDriver driver) {
		this.driver = driver;
	}
	
	// locators
	By cookie_acceptBtn = By.xpath("//button[contains(text(),'Accept all cookies')]");
	By searchContainer = By.cssSelector("#skipToMain");
	By searchbox_field = By.xpath("//input[@name='postcode']");
	By searchBtn = By.xpath("//button[@type='submit']"); //span[contains(text(),'Search')]
	
	// actions
	public void acceptCookies() {
		driver.findElement(cookie_acceptBtn).click();
	}
	
	public void searchBox(String postcode) {
		driver.findElement(searchbox_field).sendKeys(postcode);
		driver.findElement(searchbox_field).sendKeys(Keys.ENTER);
	}
	
	public void clickSearch() throws InterruptedException {
		driver.findElement(searchBtn).click();
	}
	
	public void checkArea51() throws InterruptedException {
		Thread.sleep(5000);
		Assertions.assertTrue(driver.getCurrentUrl().contains("area51") || driver.getCurrentUrl().contains("ar51"));
	}
}
