Instructions about test setup:
- I made a very simple POM (Page Object Model) design for this automation framework. It is Java-Selenium and with Maven, TestNG and JUnit dependencies (pom.xml).
- Under src/test/java, I created two packages. pageObjects package for the locators and actions and test package for the test case/script. 
- The setup of the opening and closing of browser is included in the test script.
- I added the @Test annotation to identify that which test cases to execute. 

Via the zip file:
- Extract the zip file and copy or import the project into Java IDE (Eclipse) 
*This is a maven project

Via cloning into local:
- Go to: https://gitlab.com/tamapacpac/seleniumtestv1
- Clone with https: https://gitlab.com/tamapacpac/seleniumtestv1.git
- Import the project into Java IDE (Eclipse) 

How to execute the test:
- Right click the SearchPageTest.java file and run it as "Maven Test"
Another way to run it thru cmd:
- Go to cmd > file path > mvn test
*Maven should be installed in your local computer

Some observation/s and challenge/s during the exercise: 
Observations:
	- When you input a text in the search box, the search button is not displayed, hence, when finding the element for the button it is not visible. 
	- When you click outside of the search box, the search button will be displayed again. 
	- When you hit enter, instead of clicking search button, it is automatically redirected to the searched input. 
Challenges:
- Programming language: since I am using Python and Javascript recently, it was a refresher again for me to use Java and its selenium dependencies
- Cookies: at first it was a little overwhelming but good thing I was able to easily find the locator.
